FROM python:3.6

ENV TERM xterm-256color

RUN apt-get update \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install colorama
RUN pip3 install splunk-appinspect
ADD colors.py /colors.py

CMD ["bash", "-l"]
